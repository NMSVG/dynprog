#https://www.youtube.com/watch?v=aw9wHbFTnAQ 

# makefile version 1.0.09.08.2018

CFLAGS=-std=c++17 -c -g -Wall
CC=g++
	
fib: fib.o
	@printf "\033[36mLinking \"fib\"...\n\033[0m"
	$(CC) fib.o -o fib.out
	@printf "\n\033[34mRun by typing 'make run'\n\n\033[0m"

fib.o: fib.cpp
	@printf "\033[36mCompiling \"fib\"...\n\033[0m"
	$(CC) $(CFLAGS) fib.cpp

# ...................................................................

run:
	@./fib.out

# ...................................................................

# remove temp files

clean:
	rm -f *.out *.o 


